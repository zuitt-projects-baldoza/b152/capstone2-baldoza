const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


//User Registration:
router.post("/",userControllers.registerUser);

router.get('/',userControllers.getAllUsersController);

router.post('/login',userControllers.loginUser);

router.put("/updateToAdmin/:id",verify,verifyAdmin,userControllers.updateToAdmin);

router.get('/getUserDetails',verify,userControllers.getUserDetails);

module.exports = router;


