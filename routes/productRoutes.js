const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth; 

router.post("/",verify,verifyAdmin,productControllers.registerProduct);

router.get('/',verify,verifyAdmin,productControllers.getAllProductsController);

router.get('/active',productControllers.getAllActiveProducts);

router.get('/getSingleProduct/:id',productControllers.getSingleProduct);

router.put("/:id",verify,verifyAdmin,productControllers.updateProduct);

router.put("/archive/:id",verify,verifyAdmin,productControllers.archiveProduct);

router.put("/activate/:id",verify,verifyAdmin,productControllers.activateProduct);

router.get("/:id",verify,productControllers.getProductsPerOrder);

module.exports = router;
