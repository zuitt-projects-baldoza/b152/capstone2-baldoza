const Product = require("../models/Products");
const Order = require("../models/Order");
const User = require("../models/User");

module.exports.registerProduct = (req,res) => {

console.log(req.body);

let newProduct = new Product ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	});

	newProduct.save()
	.then(product=>res.send(product))
	.catch(err => res.send(err));
	// res.send("Test route");
};

module.exports.getAllProductsController=(req,res)=>{
		Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getAllActiveProducts=(req,res)=>{
		Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleProduct=(req,res)=>{
	Product.findById(req.params.id)
	.then(result=> res.send(result))
	.catch(error=> res.send(error));
};

module.exports.updateProduct=(req,res)=>{
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	
	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
}


module.exports.archiveProduct=(req,res)=>{
	let archive = {
		isActive:false
	}
	Product.findByIdAndUpdate(req.params.id,archive,{new:true})
	.then(archiveProduct => res.send(archiveProduct))
	.catch(err => res.send(err));
}

//stretch goals
module.exports.activateProduct=(req,res)=>{
	let archive = {
		isActive:true
	}
	Product.findByIdAndUpdate(req.params.id,archive,{new:true})
	.then(activateProduct => res.send(activateProduct))
	.catch(err => res.send(err));
}

module.exports.getProductsPerOrder=(req,res)=>{
	Product.findById(req.params.id)
	.then(result=> res.send(result))
	.catch(error=> res.send(error));
};


