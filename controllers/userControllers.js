
const User = require("../models/User");

const Products = require("../models/Products");

const Order = require("../models/Order");

const bcrypt = require("bcrypt");

const auth = require("../auth");
// console.log(auth);

module.exports.registerUser = (req,res) => {

console.log(req.body);

const hashedPW = bcrypt.hashSync(req.body.password,10);
// console.log(hashedPW);
let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user=>res.send(user))
	.catch(err => res.send(err));
	// res.send("Test route");
}

module.exports.getAllUsersController=(req,res)=>{
		User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.loginUser = (req,res) => {
	//Does this controller need user input?
	//yes
	//Where can we get this user input?
	//req.body
	console.log(req.body);


User.findOne({email: req.body.email})
.then(foundUser => {
	console.log(foundUser);

	if(foundUser === null){
		return res.send({message:"No User Found."})
	} else {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
		console.log(isPasswordCorrect);
	
	if(isPasswordCorrect){
		/*
		*/
		return res.send({accessToken: auth.createAccessToken(foundUser)})
	} else {
		return res.send({message:"Incorrect Password."})
		}
	}
})
.catch(err=>res.send(err));

};

module.exports.updateToAdmin=(req,res)=>{
	console.log(req.user)
	console.log(req.params.id)

	User.findByIdAndUpdate(req.params.id,{isAdmin:true},{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

module.exports.getUserDetails=(req,res)=>{

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
	//res.send("testing for verify");
}


