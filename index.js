const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://supeeerb:mongoDB@cluster0.tranx.mongodb.net/capstone2?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error",console.error.bind(console,"Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

const corsOptions = {
	origin: "http://localhost:3000",
	optionsSuccessStatus: 200
}

app.use(cors(corsOptions));
app.use(express.json());

//User
//import route from userRoutes
const userRoutes = require('./routes/userRoutes')
//use our routes and group together under '/users'
app.use('/users',userRoutes);

//Products
const productRoutes = require('./routes/productRoutes')
app.use('/products',productRoutes);

//Orders
const orderRoutes = require('./routes/orderRoutes')
app.use('/orders',orderRoutes);

app.listen(port,()=>console.log(`Server running at port ${port}`));