/*module to authorize users to access or disallow
access to certain parts/features of app */

//imports
const jwt = require("jsonwebtoken");
const secret = "E-commerceBookingApi";

module.exports.createAccessToken = (user) => {
	// console.log(user)
const data = {
	id: user._id,
	email: user.email,
	isAdmin: user.isAdmin
}
	// console.log(data);
	return jwt.sign(data,secret,{});
}

module.exports.verify = (req,res,next) => {
	//console.log(req.headers.authorization);
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	} else {
		//next() will let us proceed to the next middleware OR controller
		token = token.slice(7,token.length);
		// console.log(token)
		// next();
		jwt.verify(token,secret,function(err,decodedToken){
			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				});
			} else {
				//console.log(decodedToken);
				req.user = decodedToken;

				// next() will let us proceed to the next middleware OR controller
				next();
			}
		})
	}
}

//verifyAdmin will also be used a middleware.
module.exports.verifyAdmin = (req,res,next) => {

	// console.log(req.user);

	if(req.user.isAdmin){
		//if the logged in user, based on his token is an admin, we will proceed to the next middleware/controller
		next();
	} else {
		return res.send({

			auth:"Failed",
			message: "Action Forbidden"
			
		})
	}

}






